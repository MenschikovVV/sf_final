# Helm

### Конвертация docker-compose в k8s (https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)

Установка kompose
~~~
curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.0/kompose-linux-amd64 -o kompose
~~~

Конвертация
~~~
kompose convert
~~~

