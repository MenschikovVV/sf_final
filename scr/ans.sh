#!/bin/bash
cd ../ans
echo "1. Установка зависимостей"
ansible-playbook -i ../tf/inventory.ini depend.yml
echo "2. Установка Docker"
ansible-playbook -i ../tf/inventory.ini docker.yml
echo "3. Установка Gitlab-runner"
ansible-playbook -i ../tf/inventory.ini gitlab_runner.yml
echo "4. Установка Kubectl"
ansible-playbook -i ../tf/inventory.ini kubectl.yml
echo "5. Установка Yandex CLI"
ansible-playbook -i ../tf/inventory.ini yc.yml
echo "6. Установка Helm"
ansible-playbook -i ../tf/inventory.ini helm.yml

