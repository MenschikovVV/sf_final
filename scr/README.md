# Скрипты

| № п/п | Название скрипта      | Описание                     |
|-------|-----------------------|------------------------------|
| 1     | [terraform](tf.sh)    | Развертывание инфраструктуры |
| 2     | [ansible](ans.sh)     | Установка приложений         |
| 3     | [ans_ing](ans_ing.sh) | Установка Ingress            |
| 4     | [ans_mon](ans_mon.sh) | Установка Мониторинга        |
| 5     | [ans_log](ans_log.sh) | Установка Логирования        |

## Примечание
При запуске скриптов, нужно находиться в директории scr