resource "yandex_compute_instance" "vm_srv" {
  name = "final-srv"

  resources {
    cores  = 4
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.image_ubuntu.id
      size = 40
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.final_sub_srv.id
    nat       = true               # Внешний IP
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/final_srv.pub")}"
  }
}