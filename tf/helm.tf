#=========== Развертывание Ingress

#resourceresource "helm_release" "k8s_ingress" {
#  name             = "k8s-ingress"
#  namespace        = "ingress"
#  repository       = "oci://cr.yandex/yc-marketplace/yandex-cloud/yc-alb-ingress"
#  chart            = "yc-alb-ingress-controller-chart"
#  version          = "v0.1.24"
#  create_namespace = true
#
#  values = [
#    <<-EOF
#    folderId: ${var.folder_id}
#    clusterId: ${yandex_kubernetes_cluster.k8s_clu.id}
#    daemonsetTolerations:
#      - operator: Exists
#    auth:
#      json: ${jsonencode(
#      {
#        "id" : yandex_iam_service_account_key.k8s_clu_ing_ak.id,
#        "service_account_id" : yandex_iam_service_account_key.k8s_clu_ing_ak.service_account_id,
#        "created_at" : yandex_iam_service_account_key.k8s_clu_ing_ak.created_at,
#        "key_algorithm" : yandex_iam_service_account_key.k8s_clu_ing_ak.key_algorithm,
#        "public_key" : yandex_iam_service_account_key.k8s_clu_ing_ak.public_key,
#        "private_key" : yandex_iam_service_account_key.k8s_clu_ing_ak.private_key
#      }
#    )}
#  EOF
#  ]
#
#  depends_on = [
#    yandex_kubernetes_cluster.k8s_clu,
#    yandex_resourcemanager_folder_iam_member.k8s_ing_sa_roles,
#    yandex_iam_service_account_key.k8s_clu_ing_ak,
#    kubernetes_namespace.ingress,
#    kubernetes_secret.ingress_sa_key,
#  ]
#}