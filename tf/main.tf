#=========== настройки провайдера ==============

terraform {
  required_version = "= 1.6.6"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.105.0"                        # Фиксируем версию провайдера
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.12.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }

  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }
    bucket = "cloud-tfstate"
    region = "ru-central1"
    key    = "sf_final/tf/terraform.tfstate"
    shared_credentials_file = "storage.key"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true # необходимая опция Terraform для версии 1.6.1 и старше.
    skip_s3_checksum            = true # необходимая опция при описании бэкенда для Terraform версии 1.6.3 и старше.
  }
}

# Документация к настройке провайдера yandex.cloud
#https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs#configuration-reference
#https://terraform-provider.yandexcloud.net/index
provider "yandex" {
  service_account_key_file = file("~/key.json")
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
  zone                     = var.zone-1
}


