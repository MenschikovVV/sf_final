#=========== создание кластера ==============

resource "yandex_kubernetes_cluster" "final_clu" {
  name        = "final-cluster"
  description = "final"

  network_id = "${yandex_vpc_network.final_net.id}"

  master {
    version = "1.26"
    zonal {
      zone      = "${var.zone-1}"
      subnet_id = "${yandex_vpc_subnet.final_sub_clu.id}"
    }

    public_ip = true

#    security_group_ids = ["${yandex_vpc_security_group.final_sg.id}"]

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }

    master_logging {
      enabled = true
      log_group_id = "${yandex_logging_group.final_log_group.id}"
      kube_apiserver_enabled = true
      cluster_autoscaler_enabled = true
      events_enabled = true
      audit_enabled = true
    }
  }

  service_account_id      = "${yandex_iam_service_account.final_clu_sa.id}"
  node_service_account_id = "${yandex_iam_service_account.final_gr_sa.id}"

  labels = {
    my_key       = "final"
  }

  release_channel = "RAPID"
#  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.final_kms_key.id
  }
  depends_on = [
    yandex_resourcemanager_folder_iam_member.final_clu_sa_roles,
  ]
}