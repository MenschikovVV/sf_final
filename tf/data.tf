#=========== Внешние ресурсы ==============

data "yandex_compute_image" "image_ubuntu" {
  family = var.instance_ubuntu_image
}

data "template_file" "ansible_inventory" {
  template = file("${path.module}/inventory/inventory.ini.tpl") # Путь до шаблона на локальном компьютере
  vars = {
    vm_srv= yandex_compute_instance.vm_srv.network_interface.0.nat_ip_address
    ext_ip= yandex_vpc_address.final_ext_ip.external_ipv4_address[0].address
    ansible_ssh_private_key_file_srv = "~/.ssh/final_srv"
    ansible_ssh_private_key_file_mas = "~/.ssh/final_master"
    token_gl= var.token_gl
    token_yc= var.token_yc
    folder_id= yandex_kubernetes_cluster.final_clu.folder_id
    cluster_id= yandex_kubernetes_cluster.final_clu.id
  }
}