[vm_srv]
${vm_srv} ansible_ssh_private_key_file=${ansible_ssh_private_key_file_srv}

[all:vars]
vm_srv = ${vm_srv}
ext_ip = ${ext_ip}
token_gl = ${token_gl}
token_yc = ${token_yc}
folder_id = ${folder_id}
cluster_id = ${cluster_id}