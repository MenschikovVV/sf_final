#=========== Ключ для шифрования секретов
resource "yandex_kms_symmetric_key" "final_kms_key" {
  name              = "final-kms-key"
  description       = "Final KMS ключ"
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
}
