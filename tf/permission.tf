#=========== доступы final_clu_sa ==============

# Даем права на роли
resource "yandex_resourcemanager_folder_iam_member" "final_clu_sa_roles" {
  folder_id = var.folder_id
  for_each  = toset([
    "k8s.clusters.agent",
    "k8s.tunnelClusters.agent",
    "vpc.publicAdmin",
    "load-balancer.admin",
    "logging.writer",
  ])
  role       = each.value
  member    = "serviceAccount:${yandex_iam_service_account.final_clu_sa.id}"
  sleep_after = 5
}

#=========== доступы final_gr_sa ==============

resource "yandex_resourcemanager_folder_iam_member" "final_gr_sa_roles" {
  folder_id = var.folder_id
  for_each  = toset([
    "container-registry.images.puller",
    "kms.keys.encrypterDecrypter",
  ])
  role       = each.value
  member    = "serviceAccount:${yandex_iam_service_account.final_gr_sa.id}"
  sleep_after = 5
}
#=========== доступы final_ing_sa ==============

resource "yandex_resourcemanager_folder_iam_member" "final_ing_sa_roles" {
  folder_id = var.folder_id
  for_each = toset([
    "alb.editor",
    "vpc.publicAdmin",
    "certificate-manager.certificates.downloader",
    "compute.viewer",
  ])
  role       = each.value
  member     = "serviceAccount:${yandex_iam_service_account.final_ing_sa.id}"
  depends_on = [
    yandex_iam_service_account.final_ing_sa,
  ]
  sleep_after = 5
}

# Создаем ключи доступа Service Account Key
resource "yandex_iam_service_account_key" "final_clu_ing_ak" {
  service_account_id = yandex_iam_service_account.final_ing_sa.id
  depends_on         = [
    yandex_iam_service_account.final_ing_sa,
  ]
}
