# Опиcание инфраструктуру

## Файлы

| наименование                   | описание                              |
|--------------------------------|---------------------------------------|
| [cluster.tf](cluster.tf)       | Кластер                               |
| [data.tf](data.tf)             | Внешние ресурсы                       |
| [group.tf](group.tf)           | Группа узлов                          |
| [inventory.tf](inventory.tf)   | Заполнение шаблона для ansible        |
| [k8s](k8s.tf)                  | Создание ресурсов k8s                 |
| [kms](kms.tf)                  | Ключ для шифрования секретов          |
| [logging](logging.tf) | Cloud Logging |
| [main.tf](main.tf)             | Настройка провайдера                  |
| [output.tf](output.tf)         | Возвращаемые параметры                |
| [permission.tf](permission.tf) | Присвоение ролей сервис аккаунтам     |
| [sa.tf](sa.tf)                 | Создание сервис аккаунтов             |
| [variables.tf](variables.tf)   | Описание переменных                   |
| [vm.tf](vm.tf)                 | Настройка ВМ                          |
| [vpc.tf](vpc.tf)               | Настройка сети                        |
| **terraform.tfvars**           | Переменные окружения                  |
| **storage.key**                | Доступ к хранилищу                    |
| **~/.ssh/final_srv**           | Сертификат для доступа к сервисной ВМ |
| **~/.ssh/final_master**        | Сертификат для доступа к кластеру     |

## Дополнительно

Требуется создать файл **terraform.tfvars** следующего вида:
~~~
cloud_id = "<Cloud ID>"
folder_id = "<Folder ID>"

# Для получения в GitLab: Settings->CI/CD->Runners->New project runner
token_gl = "<токен gitlab runner>"

# Для получения https://oauth.yandex.ru/authorize?response_type=token&client_id=<Ид клиента>
token_yc = "<токен Yandex>"
~~~

Требуется создать файл **storage.key** следующего вида:
~~~
[default]
    aws_access_key_id = <Идентификатор ключа доступа к сервисному аккаунту>
    aws_secret_access_key = <Ваш секретный ключ доступа к сервисному аккаунту>
~~~

Необходимо сгенерировать SSH ключи
~~~
ssh-keygen  -P "" -f ~/.ssh/final_srv
ssh-keygen  -P "" -f ~/.ssh/final_master
~~~