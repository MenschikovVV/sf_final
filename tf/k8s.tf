#=========== Создание объектов k8s

## Namespace
#resource "kubernetes_namespace" "ingress" {
#  metadata {
#    name = "ingress"
#  }
#}
## Secret
#resource "kubernetes_secret" "ingress_sa_key" {
#  metadata {
#    name      = "ingress-controller-sa-key"
#    namespace = "ingress"
#  }
#  data = {
#    "sa-key.json" = jsonencode(
#      {
#        "id" : yandex_iam_service_account_key.k8s_clu_ing_ak.id,
#        "service_account_id" : yandex_iam_service_account_key.k8s_clu_ing_ak.service_account_id,
#        "created_at" : yandex_iam_service_account_key.k8s_clu_ing_ak.created_at,
#        "key_algorithm" : yandex_iam_service_account_key.k8s_clu_ing_ak.key_algorithm,
#        "public_key" : yandex_iam_service_account_key.k8s_clu_ing_ak.public_key,
#        "private_key" : yandex_iam_service_account_key.k8s_clu_ing_ak.private_key
#      }
#    )
#  }
#
#  type       = "kubernetes.io/Opaque"
#  depends_on = [
#    kubernetes_namespace.ingress
#  ]
#}