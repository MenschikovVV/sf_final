#=========== параметры вывода ==============

# ID сети
output "network_id" {
  description = "ID сети"
  value = yandex_vpc_network.final_net.id
}

# ID подсети subnet k8s
output "subnet_k8s_id" {
  description = "ID подсети final_subnet_cluster"
  value = yandex_vpc_subnet.final_sub_clu.id
}

# ID подсети subnet k8s
output "subnet_srv_id" {
  description = "ID подсети final_subnet_service"
  value = yandex_vpc_subnet.final_sub_srv.id
}

# external IP ВМ
output "external_ip_address_vm_srv" {
  value = yandex_compute_instance.vm_srv.network_interface.0.nat_ip_address
}
