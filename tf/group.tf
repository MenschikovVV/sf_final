resource "yandex_kubernetes_node_group" "final_group" {
  cluster_id  = "${yandex_kubernetes_cluster.final_clu.id}"
  name        = "final-group"
  description = "final"
  version     = "1.26"

  labels = {
    "key" = "final"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      ipv4               = true
      nat                = false          # Внешний IP
      subnet_ids         = ["${yandex_vpc_subnet.final_sub_clu.id}"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = var.zone-1
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_member.final_gr_sa_roles,
  ]
}