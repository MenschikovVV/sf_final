#=========== Создаем сервис-аккаунт SA ==============
resource "yandex_iam_service_account" "final_clu_sa" {
  folder_id = var.folder_id
  name      = "final-cluster-sa"
}

resource "yandex_iam_service_account" "final_gr_sa" {
  folder_id = var.folder_id
  name      = "final-group-sa"
}

resource "yandex_iam_service_account" "final_ing_sa" {
  folder_id   = var.folder_id
  name        = "final-ingress-sa"
}
