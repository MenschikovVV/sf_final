#=========== создание сети ==============

resource "yandex_vpc_network" "final_net" {
  name = var.network_name
}

#=========== создание подсетей ==============

resource "yandex_vpc_subnet" "final_sub_clu" {
  name           = "final_subnet_cluster"
  zone           = var.zone-1
  network_id     = yandex_vpc_network.final_net.id
  v4_cidr_blocks = ["192.168.150.0/24"]
  route_table_id = yandex_vpc_route_table.final_route.id
}

resource "yandex_vpc_subnet" "final_sub_srv" {
  name           = "final_subnet_srv"
  zone           = var.zone-1
  network_id     = yandex_vpc_network.final_net.id
  v4_cidr_blocks = ["192.168.151.0/24"]
}

#=========== создание шлюза ==============

resource "yandex_vpc_gateway" "default" {
  name = "default"
  shared_egress_gateway {}
}

#=========== создание маршрутов ==============

resource "yandex_vpc_route_table" "final_route" {
  name       = "final-route"
  network_id = yandex_vpc_network.final_net.id
  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.default.id
  }
}

#=========== создание внешнего IP  ==============

resource "yandex_vpc_address" "final_ext_ip" {
  name = "final-external-ip"

  external_ipv4_address {
    zone_id = var.zone-1
  }
}

#=========== создание security group  ==============

resource "yandex_vpc_security_group" "final_sg" {
  name        = "final-security_group"
  description = "final"
  network_id  = yandex_vpc_network.final_net.id
  folder_id   = var.folder_id

  ingress {
    protocol       = "ICMP"
    description    = "ping"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol       = "TCP"
    description    = "http"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "https"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }

  ingress {
    protocol          = "TCP"
    description       = "Rule allows availability checks from load balancer's address range. It is required for a db cluster"
    predefined_target = "loadbalancer_healthchecks"
    from_port         = 0
    to_port           = 65535
  }

  ingress {
    protocol          = "ANY"
    description       = "Rule allows master and slave communication inside a security group."
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }

  egress {
    protocol       = "TCP"
    description    = "Enable traffic from ALB to K8s services"
    v4_cidr_blocks = yandex_vpc_subnet.final_sub_clu.v4_cidr_blocks
    from_port      = 30000
    to_port        = 65535
  }

  egress {
    protocol       = "TCP"
    description    = "Enable probes from ALB to K8s"
    v4_cidr_blocks = yandex_vpc_subnet.final_sub_clu.v4_cidr_blocks
    port           = 10501
  }
}
