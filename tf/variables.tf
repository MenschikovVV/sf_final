#=========== main ==============

variable "cloud_id" {
  description = "The cloud ID"
  type        = string
}

variable "folder_id" {
  description = "The folder ID"
  type        = string
}

variable "zone-1" {
  description = "Use zone - ru-central1-a"
  type        = string
  default     = "ru-central1-a"
}

#=========== network ==============
variable "network_name" {
  description = "The name of main network"
  type        = string
  default     = "final_net"
}

#=========== image ==============

variable "instance_ubuntu_image" {
  description = "Instance image ubuntu 22.04LTS"
  type        = string
  default     = "ubuntu-2204-lts"
}

#============ token GL ==============
variable "token_gl" {
  description = "Token GitLab Runner"
  type        = string
}

#============ token YC ==============
variable "token_yc" {
  description = "Token Yandex"
  type        = string
}