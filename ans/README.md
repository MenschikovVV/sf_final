# Ansible

1. Установка [Depend](depend.yml)
2. Установка [Docker](docker.yml)
3. Установка [kubectl](kubectl.yml)
4. Установка [gitlab_runner](gitlab_runner.yml)
5. Установка [yc](yc.yml)
6. Установка [helm](helm.yml)
7. Нужно зайти на vm_srv по ssh и выполнить
- для пользователя ubuntu 
~~~
# Инициализация yandex cli
yc init
# Подключение кластера к kubectl
yc k8s cluster get-credentials final-cluster --external
~~~
- для gitlab-runner
~~~
# Поключение bash под пользователем gitlab-runner 
sudo -u gitlab-runner bash
# Установка yandex cli
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
# Инициализация yandex cli
yc init
# Подключение кластера к kubectl
yc k8s cluster get-credentials final-cluster --external
~~~

8. Установка [ingress](ingress.yml). Выполняется после инициализации Yandex CLI п.7. 
9. Установка [prometheus_grafana](prometheus_grafana.yml)